/**
 * Version 1.0, Jan 2012
 */

var Markit = {};
/**
* Define the QuoteService.
* First argument is symbol (string) for the quote. Examples: AAPL, MSFT, JNJ, GOOG.
* Second argument is fCallback, a callback function executed onSuccess of API.
*/
Markit.QuoteService = function(sSymbol, fCallback) {
    this.symbol = sSymbol;
    this.fCallback = fCallback;
    this.DATA_SRC = "http://dev.markitondemand.com/Api/v2/Quote/jsonp";
    this.makeRequest();
};

/**
* Ajax success callback. fCallback is the 2nd argument in the QuoteService constructor.
*/
Markit.QuoteService.prototype.handleSuccess = function(jsonResult) {
    this.fCallback(jsonResult);

	//This calls the table update function
	addtotable(jsonResult);
};

/**
* Ajax error callback
*/
Markit.QuoteService.prototype.handleError = function(jsonResult) {
    console.error(jsonResult);
};

/**
* Starts a new ajax request to the Quote API
*/
Markit.QuoteService.prototype.makeRequest = function() {
    //Abort any open requests
    if (this.xhr) { this.xhr.abort(); }
    //Start a new request
    this.xhr = $.ajax({
        data: { symbol: this.symbol },
        url: this.DATA_SRC,
        dataType: "jsonp",
        success: this.handleSuccess,
        error: this.handleError,
        context: this
    });
};

function requestStock(symbol) {
    new Markit.QuoteService(symbol, function(jsonResult) {
        if (!jsonResult || jsonResult.Message) {
            console.error("Error: ", jsonResult.Message);
            return;
        }
        //console.log(jsonResult);

		return(jsonResult);
    });
}


//adds the returned stock to the table async - This is called by Markit.QuoteService.prototype.handleSuccess event
function addtotable(data) {
	$('#stockstable tr:last').after('<tr><td>'+data.Status+'</td><td>'+data.Name+'</td><td>'+data.LastPrice+'</td><td>'+data.ChangePercent+'</td></tr>');
}
