<!DOCTYPE html>
<html>
<head>
	<title>Stocks</title>

	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/quoteservice.js"></script>
	<script type="text/javascript" src="js/stock.js"></script>

	<script>

		//The problem here is that you are trying to put the data into the variable synchronously while the ajax call executes async, so it dosent have anything to populate the stock var with when it gets there... see the button and js file for more.
		//var stock = requestStock("AAPL");
	</script>
</head>
<body>

<table id='stockstable' style='width:100%;' border='black'>
<tr>
<th>Status
</th>
<th>Name
</th>
<th>LastPrice
</th>
<th>ChangePercent
</th>
</tr>

<input id='tickerbox' type='textbox' value='Insert ticker here' />
<button id='go' onclick='requestStock(document.getElementById("tickerbox").value);' >Go!</button>





</body>

</html>